// swatch settings
width=3.5*25.4; // overall width
height=2*25.4; // overall height
thickness=3; // overall thickness
round=3; // corner radius
stepsize=0.2; // thickness gauge thickness step (should be multiple of layer height)
steps=14; // number of thickness gauge steps (stepsize*steps should <=thickness)

// filament settings
filament_brand="Dazzle Light";
filament_color="purple";
filament_type="PLA";
hotend_temp=210;
bed_temp=60;

// these shouldn't need adjustment
textarea_width=width-.2*25.4;
textarea_height=height-.8*25.4;
textarea_depth=.6;
font="Arial:style=Bold";
$fn=45;

difference()
{
    // main body
    translate([round, round, 0])
    minkowski()
    {
        cylinder(r=round, h=thickness/2);
        cube([width-2*round, height-2*round, thickness/2]);
    }

    // cut out text area
    translate([.1*25.4, .7*25.4, thickness-textarea_depth])
    translate([round, round, 0])
    minkowski()
    {
        cylinder(r=round, h=1);
        cube([textarea_width-2*round, textarea_height-2*round, textarea_depth]);
    }

    // cut out thickness gauge
    translate([.1*25.4, .1*25.4, 0])
        cube([width-.2*25.4, .5*25.4, thickness+.1]);

}

// render thickness gauge and its label
for (i=[0:1:steps-1])
{
    translate([.1*25.4+i*((width-.2*25.4)/steps), .1*25.4, 0])
        cube([(width-.2*25.4)/steps, .5*25.4, (i+1)*stepsize]);
    translate([.1*25.4+(i+.5)*((width-.2*25.4)/steps), .75*25.4, thickness-textarea_depth])
    linear_extrude(textarea_depth, convexity=10)
    rotate([0, 0, 90])
        text(str((i+1)*stepsize), size=3.5, valign="center", font=font);
}

// render filament info
translate([width/2, height-8, thickness-textarea_depth])
linear_extrude(textarea_depth, convexity=10)
    text(filament_brand, size=5, halign="center", font=font);
translate([width/2, height-15, thickness-textarea_depth])
linear_extrude(textarea_depth, convexity=10)
    text(str(filament_color, " ", filament_type), size=5, halign="center", font=font);
translate([width/2, height-21, thickness-textarea_depth])
linear_extrude(textarea_depth, convexity=10)
    text(str("hotend=", hotend_temp, "°C, bed=", bed_temp, "°C"), size=4, halign="center", font=font);
