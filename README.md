Filament Swatch Generator
=========================

By default, this creates a business-card-sized swatch 3 mm thick, with a
thickness gauge from 0.2 to 2.8 mm.  Labels can be set to identify the
filament in use.  Use something like this to generate a swatch:

```
openscad filament_swatch.scad -o swatch.stl \
  -D 'filament_brand="Veeology"' \
  -D 'filament_color="copper"' \
  -D 'filament_type="silk PLA"' \
  -D 'hotend_temp="210"' \
  -D 'bed_temp="60"'
```

For printing, it is recommended that the first-layer thickness is set equal
to the default extrusion width.  Also, it may be necessary to print the text
with a thinner extrusion width for it to be fully rendered; in PrusaSlicer,
you can use a height modifier from 2.6 mm (default) on up to set the
external perimeter extrusion width to 0.25 mm.

